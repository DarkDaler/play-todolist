Práctica 2 - Pruebas en Play Framework
=========

En esta segunda práctica el objetivo principal es entencer el funcionamiento de las pruebas en Play framework. Dividiremos la práctica en 2 partes:

-La primera será la realización de todas las pruebas pertinentes para testear las 3 features de la práctica 1. Habrá que conseguir unos tests solidos capaces de testear todos los casos posibles creados.

-La segunda será la creación de una nueva caracteristica para la API, que dará la posibilidad de crear categorias para un usuario, y de asignar a las tareas dicha categoria con la posibilidad de luego listarlas o modificarlas pasando dicha caracteristica por parametro.

PARTE 1
----

El testeo de nuestra práctica 1 nos dará los siguientes casos:

#### -Feature1 Modelo:
    "Test tasks vacias FEATURE 1" in { 
Test en el que se listan las tareas sin crear anteriormente ninguna.

    "Test busqueda task en vacia FEATURE 1" in {
Test de busqueda de una id de una tarea no creada aun.
    
    "Test crear una task y checkear que se ha creado FEATURE 1" in {
Test en el cual se crea una tarea y se lista para ver si se ha creado.
    
    "Test checkear con getTasks(id) una tarea creada FEATURE 1" in {
Test en el cual se crea una tarea y se busca mediante el id.
    
    "Test crear varias tareas FEATURE1" in {
Test en el cual se crean 4 tareas y se hace un listar todas para ver que se hayan creado.
    
    "Test crear y eliminar una tarea FEATURE1" in {
Test en el que se crea una tarea y se elimina luego para ver que se elimina correctamente
    
    "Test eliminar una tarea inexistente FEATURE1" in {
Test en el que se intenta eliminar una tarea que no existe.
    

#### -Feature1 Aplicación:
    "Envio 404 de peticion erronea" in {
Test en el cual se testea que una ruta incorrecta devuelve el error `404 NOT FOUND`.

    "Prueba pagina index" in {
Test en el cual se pide la ruta index /.

    "Prueba pagina task vacia FEATURE 1" in {
Test en el cual se mira que la ruta /task devuelve un json vacio.

    "Prueba introducir 1 tarea FEATURE 1" in {
Test en el cual se introduce una tarea y se ve que devuelve el JSON correcto.

    "Prueba introducir varias tareas FEATURE 1" in {
Test en el cual se introducen 4 tareas y mediante la peticion `GET` captura todas en formato JSON.

    "Prueba getTask(id) de tarea existente FEATURE1" in {
Test en el cual se introduce 1 tarea y se intenta recoger mediante la ruta `/tasks/1` captura el JSON de dicha tarea.

    "Prueba getTask(id) de tarea no existente FEATURE1" in {
Test en el cual se intenta captar una tarea inexistente por id.

    "Prueba a eliminar una tarea inexistente FEATURE1" in {
Test en el que se intenta eliminar una tarea que no existe.

#### -Feature2 Modelo:
    "Test task index vacia al insertarle en usuario registrado" in {
Test que inserta una tarea al usuario admin y checkea que `all()` no devuelve nada.

    "Test inserta 2 task a admin hacer getTaskUser(user)" in {
Test que inserta 2 tareas al usuario admin y 1 a anonimo y mira que las de admin sean las correctas.

    "Test eliminar una task a usuario registrado" in {
Test que elimina una tarea a un usuario registrado y ckeckea que se ha eliminado correctamente.

#### Feature2 Aplicación:
    "Prueba tasks vacias de user admin y anonimo FEATURE 2" in{
Test que ckeckea que el admin y anonimo no tienen ninguna tarea mediante la petición `/users/:login/tasks`

    "Prueba insertar un usuario en admin FEATURE 2" in {
Test que inserta una tarea a admin y ckeckea que anonimo no tiene ninguna tarea y admin 1 en formato JSON.

    "Prueba buscar tareas de un usuario no registrado FEATURE2" in {
Test que checkea que la peticion `GET` sobre un usuario no registrado devuelve el HTML CODE `NOT FOUND`

    "Prueba insertar 2 usuarios en admin y 1 en anonimo FEATURE2" in {
Test que inserta 2 usuarios en admin y 1 en anonimo y testea las respectivas tareas en formato JSON.

    "Prueba insertar 2 en admin, 2 en anonimo, y eliminar 1 de admin FEATURE2" in {
Test que inserta 2 tareas a admin, 2 a anonimo, elimina 1 a admin, y checkea que se ha eliminado correctamente en formato JSON.

#### -Feature3 Modelo:
    "Test index vacio al insertar tarea de usuario con fecha" in {
Test en el cual se inserta una tarea con fecha en el usuario admin y se checkea `all()` para ver que esta vacio.

    "Test inserta 2 task a admin y 1 a anonimo con fecha y hacer getTaskUserDate(user, date)" in {
Test que inserta 2 tareas a admin y 1 a anonimo y se chequea que existan mediante `getTaskUserData(USER, DATE)`

    "Test eliminar una task a usuario registrado y que tenga fecha" in {
Test que elimina una tarea a un usuario registrado y de una fecha, y despues se testea que no exista ninguna tarea.

    "Test insertar 2 task con fecha a usuario registrado y hacer getTaskUser(user)" in {
Test que inserta 2 tareas al usuario admin y que `getTaskUser(USER)` devuelve estas 2 tareas.

#### -FEATURE3 Aplicacion:
    "Prueba insertar una task para admin FEATURE 3" in{
Test en el cual se inserta una tarea con fecha al usuario admin y se verifica mediante JSON

    "Prueba insertar una task con una fecha de formato incorrecto FEATURE 3" in{
Test en el que se intenta insertar una tarea con una fecha de formato incorrecto y devuelve el HTML STATUS `BAD REQUEST`.

    "Prueba insertar una task con una fecha con día incorrecto FEATURE 3" in{
Test en el que se inserta una tarea con una fecha con un día incorrecto y devuelve el HTML STATUS `BAD REQUEST`.

    "Prueba insertar una task con una fecha con mes incorrecto FEATURE 3" in{
Test en el que se inserta una tarea con mes incorrecto y devuelve el HTML STATUS `BAD REQUEST`.

    "Prueba insertar una task con una fecha con user incorrecto FEATURE 3" in{ 
Test en el cual se inserta una tarea a un user no registrado.

    "Prueba insertar 2 usuarios en admin y 1 en anonimo FEATURE3" in {
Test en el que se inserta 2 tareas a admin y 1 a anonimo con fecha y checkea que está correcto en formato JSON.

    "Prueba insertar 2 en admin, 2 en anonimo, y eliminar 1 de admin FEATURE3" in {
Test en el cual se inserta 2 tareas en admin, 2 en anonimo y elimina 1 de admin, y checkea en formato JSON que está correcto.

PARTE 2
-----------

La nueva característica, categoria, se implementará mediante TDD. Esto significará que tendremos que crear primeramente el test, y conseguir que este nos de como correcto despues de implementar los códigos pertinentes.

#### -Capa Modelo:

    "Test listar todas las categorias disponibles de admin vacio" in {
Test que lista todas las categorias de un usuario, en concreto la de admin.

    def listarCategorias(user: String): List[String] = {
Funcion de la capa modelo que busca en la BD el usuario pasado por parametros y lista las categorias asignadas a el.

    "Test crear dos categorias para admin" in {
Test que crea dos categorias para un usuario, en concreto para admin.

    def createCategoria(user: String, categoria: String){
Funcion de la capa modelo que crea una categoria y se la asigna a un usuario en la BD.

    def createCategoriaExistente(user: String, categoria: String){
Funcion de la capa modelo que asigna una categoria que ya existe en la BD a un usuario.

    "Test verificar si una categoria existe para un usuario" in {
Test que mira si existe una categoria en un usuario.

    def verifyCategoria(user: String, categoria: String): Long = {
Funcion de la capa modelo que verifica si una categoria existe para un usuario.

    "Test buscar una categoria no creada para un usuario" in {
Test que verifica que no existe una categoria no existente en un usuario.

    "Test listar tareas vacias con categoria" in {
Test que mira que las task devueltas por un user en una categoria creada vacia, no devuelve nada.

    def listarTareasCategoria(idUser: String, categoriaTask: String) : List[Task] = {
Funcion de la capa modelo que lista todas las tareas que tiene un user con una categoria asignada.

    "Test crear dos tareas con una categoria para admin" in {
Test que crea dos tareas con una categoria asignada al usuario admin.

    def createTaskCategoria(label: String, user: String, categoriaTask: String){
Funcion de la capa modelo que crea tareas asignandoselas a un usuario y a una categoria.

    "Test crear una tarea para una categoria y otra para otra categoria" in {
Test que crea una tarea con una categoria en concreto y otra para otra, y las lista.

    "Test listar tareas de un usuario sin categorias y otro con categorias" in {
Test que lista tareas de un usuario que no tiene asignada ninguna categoria, y otro que si que tiene tareas asignadas.

    "Test modificar campos de una tarea con categoria de un user" in {
Test que modifica los campos de una tarea con categoria para un user.

    def modifyTaskCategoria(label: String, label2: String, idUser: String, categoriaTask: String){
Funcion que modifica el campo label de una tarea de un usuario con una categoria asignada.

#### -Capa Application:

Las rutas creadas en nuestra application son las siguientes:
    # CATEGORIA
    POST     /users/:login/categoria/:categoria   controllers.Application.newUserCat(login: String, categoria: String)
    # TASK-CATEGORIA
    PUT     /users/:login/categoria/:categoria/tasks/:label/:label2   controllers.Application.modifyTasksUserCat(label: String, label2: String, login: String, categoria: String)
    
    GET     /users/:login/categoria/:categoria/tasks   controllers.Application.getTasksUserCat(login: String, categoria: String)
    
    POST     /users/:login/categoria/:categoria/tasks   controllers.Application.newTasksUserCat(login: String, categoria: String)

    "Prueba crear categoria no existente al usuario anonimo" in {
Test que intenta crear una categoria que no existe en la BD al usuario anonimo. Devolvera `CREATED`

    def newUserCat(user: String, categoria: String) = Action {
Funcion de la capa aplicacion que crea una categoria para un usuario. Si la categoria ya existe, simplemente se la asigna. Devuelve `CREATED` en caso de que todo vaya bien, y en caso de que esa tarea ya este asignada al usuario, devuelve `BAD REQUEST`

    "Prueba crear categoria ya existente al usuario anonimo" in {
Test que crea una categoria a un usuario, y despues vuelve a crearla para

    "Prueba a insertar una categoria a 2 usuarios distintos" in {
Test que inserta unca categoria a un usuario, y despues se la inserta a otro usuario.

    "Prueba a listar tareas de una categoria vacia" in {
Test que crea una categoria para un usuario,y despues lista las tareas creadas con dicha categoria para ver que esta vacia.

    def getTasksUserCat(user: String, categoria: String) = Action {
Funcion de la capa Aplicacion que devuelve en formato JSON la lista de tareas asignadas a un user con una categoria. Devuelve `OK` si todo esta correcto. Devuelve `NOT FOUND` o `BAD REQUEST` si el usuario no existe o la categoria no existe para ese usuario.

    "Prueba a listar tareas de una categoria no existente" in {
Test que prueba que dada una categoria inexistente, devuelve `BAD REQUEST`

    "Prueba a listar tareas de un usuario no existente" in {
Test que prueba que dado un usuario inexistente, devuelve `NOT FOUND`

    "Prueba a crear 1 tarea y listarla en la categoria medicina del user anonimo" in {
Test que que crea una tarea y la lista.

    def newTasksUserCat(user: String, categoria: String) = Action { implicit request =>
Funcion de la capa aplicacion que crea una tarea para un usuario y en una categoria. Si todo va bien devuelve el estado `CREATED` y la tarea creada en formato JSON. Si el usuario no existe devuelve `NOT FOUND` y si no existe la tarea para dicho usuario devuelve `BAD REQUEST`

    "Prueba a crear 1 tarea en una categoria no existente" in {
Test que intenta crear una tarea en una categoria inexistente.

    "Prueba a crear 1 tarea en un usuario no existente" in {
Test que intenta crear una tarea a un usuario inexistente.

    "Prueba a crear 1 tarea en medicina en anonimo y 2 en informatica en admin" in {
Test que crea 1 tarea para el usuario anonimo en medicina y 2 tareas en el usuario admin con la categoria informatica.

    "Prueba a modificar una tarea existente en anonimo y en medicina" in {
Test que modifica una tarea que existe en el usuario anonimo con la categoria medicina.

    def modifyTasksUserCat(label: String, label2: String, user: String, categoria: String) = Action {
Funcion de la capa Aplicacion que modifica el label de una tarea asignada a un usuario en una categoria en concreto. Si todo sale correcto, devuelve el estado `CREATED` junto al JSON con la tarea modificada. Si no existe el usuario devuelve `NOT FOUND`, y si la categoria no existe o no esta vinculada al usuario, devuelve `BAD REQUEST`.

    "Prueba a modificar una tarea de un user que no existe" in {
Test que intenta modificar una tarea de un usuario que no existe.

    "Prueba a modificar una tarea de una categoria que no existe" in {
Test que intenta modificar una tarea de una categoria que no existe.
